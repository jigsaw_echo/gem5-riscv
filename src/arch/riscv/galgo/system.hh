#ifndef __ARCH_GALGO_SYSTEM_HH__
#define __ARCH_GALGO_SYSTEM_HH__

#include <string>
#include <vector>

#include "arch/riscv/system.hh"
#include "base/loader/symtab.hh"
#include "cpu/pc_event.hh"
#include "kern/system_events.hh"
#include "params/GalgoSystem.hh"
#include "sim/sim_object.hh"
#include "sim/system.hh"

class GalgoSystem : public RiscvSystem
{
  public:
    typedef GalgoSystemParams Params;
    GalgoSystem(Params *p);
    ~GalgoSystem();

    void initState() override;

/**
 * Serialization stuff
 */
  public:
    void serializeSymtab(CheckpointOut &cp) const override;
    void unserializeSymtab(CheckpointIn &cp) override;

    ObjectFile *rom;

  protected:
    const Params *params() const { return (const Params *)_params; }
};

#endif
