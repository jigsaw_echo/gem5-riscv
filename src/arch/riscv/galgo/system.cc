#include "arch/riscv/galgo/system.hh"

#include <sys/signal.h>

#include "arch/vtophys.hh"
#include "base/loader/object_file.hh"
#include "base/loader/symtab.hh"
#include "base/trace.hh"
#include "mem/physical.hh"
#include "params/GalgoSystem.hh"
#include "sim/byteswap.hh"

GalgoSystem::GalgoSystem(Params *p)
    : RiscvSystem(p)
{
    rom = createObjectFile(params()->rom_bin, true);
    if (rom == NULL)
        fatal("Could not load rom binary %s", params()->rom_bin);

    _resetVect = params()->reset_vect;
    inform_once("System reset: 0x%x", _resetVect);
}

void
GalgoSystem::initState()
{
    // Call the initialisation of the super class
    System::initState();

    // load program sections into memory
    rom->setTextBase(params()->reset_vect);
    rom->loadSections(physProxy);
}

GalgoSystem::~GalgoSystem() {
    delete rom;
}

void
GalgoSystem::serializeSymtab(CheckpointOut &cp) const
{
}

void
GalgoSystem::unserializeSymtab(CheckpointIn &cp)
{
}

GalgoSystem *
GalgoSystemParams::create()
{
    return new GalgoSystem(this);
}
