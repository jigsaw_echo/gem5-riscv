#ifndef __DEV_HARE_HH__
#define __DEV_HARE_HH__

#include "dev/platform.hh"
#include "params/Hare.hh"

class Hare : public Platform
{
  public:
    /** Pointer to the system */
    System *system;

  public:
    typedef HareParams Params;
    /**
     * Constructor for the Tsunami Class.
     * @param name name of the object
     * @param s system the object belongs to
     * @param intctrl pointer to the interrupt controller
     */
    Hare(const Params *p);

    /**
     * Cause the cpu to post a serial interrupt to the CPU.
     */
    virtual void postConsoleInt() override;

    /**
     * Clear a posted CPU interrupt
     */
    virtual void clearConsoleInt() override;

    /**
     * Cause the chipset to post a cpi interrupt to the CPU.
     */
    virtual void postPciInt(int line) override;

    /**
     * Clear a posted PCI->CPU interrupt
     */
    virtual void clearPciInt(int line) override;

};

#endif // __DEV_HARE_HH__
