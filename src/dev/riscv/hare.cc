#include "dev/riscv/hare.hh"

#include <deque>
#include <string>
#include <vector>

#include "cpu/intr_control.hh"
#include "sim/system.hh"

using namespace std;

Hare::Hare(const Params *p)
    : Platform(p), system(p->system)
{}

void
Hare::postConsoleInt()
{
    warn_once("Don't know what interrupt to post for console.\n");
    //panic("Need implementation\n");
}

void
Hare::clearConsoleInt()
{
    warn_once("Don't know what interrupt to clear for console.\n");
    //panic("Need implementation\n");
}

void
Hare::postPciInt(int line)
{
    panic("Need implementation\n");
}

void
Hare::clearPciInt(int line)
{
    panic("Need implementation\n");
}

Hare *
HareParams::create()
{
    return new Hare(this);
}
