from m5.params import *
from m5.proxy import *
from Device import BasicPioDevice, PioDevice, IsaFake, BadAddr
from Platform import Platform


class Hare(Platform):
    type = 'Hare'
    cxx_header = "dev/riscv/hare.hh"
    system = Param.System(Parent.any, "system")

    fake_devices = IsaFake(pio_addr=0x00000000, pio_size=0x00100000)
            #warn_access="Accessing Cores -- Unimplemented!")

    # Attach I/O devices to specified bus object.  Can't do this
    # earlier, since the bus object itself is typically defined at the
    # System level.
    def attachIO(self, bus):
        self.fake_devices.pio = bus.master
